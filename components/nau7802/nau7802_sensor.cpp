#include "nau7802_sensor.h"
#include <Wire.h>
#include "esphome/core/log.h"

namespace esphome {
  namespace nau7802_ {

    static const char *const TAG = "nau7802.sensor";

    Nau7802Sensor::Nau7802Sensor(int data_pin, int clock_pin){
        this->sda = data_pin;
        this->scl = clock_pin;
        
        Wire.begin(data_pin, clock_pin);
        if (! this->nau.begin()) {
            ESP_LOGE(TAG,"Failed to find NAU7802");
        }

        this->nau.setLDO(NAU7802_3V0);
        this->nau.setGain(NAU7802_GAIN_128);
        this->nau.setRate(NAU7802_RATE_10SPS);

        // Take 10 readings to flush out readings
        //for (uint8_t i=0; i<10; i++) {
        //    while (! nau.available()) delay(1);
        //    this->nau.read();
        //}
//
        //while (! this->nau.calibrate(NAU7802_CALMOD_INTERNAL)) {
        //    ESP_LOGI(TAG,"Failed to calibrate internal offset, retrying!");
        //    delay(1000);
        //}
        //ESP_LOGI(TAG,"Calibrated internal offset");
//
        //while (! this->nau.calibrate(NAU7802_CALMOD_OFFSET)) {
        //    ESP_LOGI(TAG,"Failed to calibrate system offset, retrying!");
        //    delay(1000);
        //}
        //ESP_LOGI(TAG,"Calibrated system offset");
    };

    // overwrites
    void Nau7802Sensor::dump_config() {
      ESP_LOGCONFIG(TAG, "Adafruit Nau7802 Sensor:");
      ESP_LOGCONFIG(TAG, "  Data Pin:  " + this->sda);
      ESP_LOGCONFIG(TAG, "  Clock Pin: " + this->scl);
    }

    void Nau7802Sensor::update() {
        while (! this->nau.available()) {
            delay(10);
        }
        int32_t val = this->nau.read();
        this->publish_state(val);
    }

    // configures
    void Nau7802Sensor::set_voltage(char* voltage){
        if (voltage == "4v5")
        {
            this->nau.setLDO(NAU7802_4V5);
        }
        else if (voltage == "4v2")
        { 
            this->nau.setLDO(NAU7802_4V2);
        }
        else if (voltage == "3v9")
        { 
            this->nau.setLDO(NAU7802_3V9);
        }
        else if (voltage == "3v6")
        { 
            this->nau.setLDO(NAU7802_3V6);
        }
        else if (voltage == "3v3")
        { 
            this->nau.setLDO(NAU7802_3V3);
        }
        else if (voltage == "3v0")
        { 
            this->nau.setLDO(NAU7802_3V0);
        }
        else if (voltage == "2v7")
        { 
            this->nau.setLDO(NAU7802_2V7);
        }
        else if (voltage == "2v4")
        { 
            this->nau.setLDO(NAU7802_2V4);
        }
        else 
        {
            this->nau.setLDO(NAU7802_EXTERNAL);
        }

        ESP_LOGI(TAG,"Voltage is set to %f",nau.getLDO());
    }

  }  // namespace nau7802_
}  // namespace esphome
