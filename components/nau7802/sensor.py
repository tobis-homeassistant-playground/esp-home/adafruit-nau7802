import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import sensor
from esphome.const import (
    CONF_DATA_PIN,
    CONF_CLOCK_PIN,
    CONF_VOLTAGE,
    CONF_GAIN,
    CONF_RATE
)
from . import nau7802_ns

Nau7802Sensor = nau7802_ns.class_("Nau7802Sensor", sensor.Sensor, cg.PollingComponent)

CONFIG_SCHEMA = cv.All(
    sensor.sensor_schema(Nau7802Sensor)
    .extend(
        {
            cv.Required(CONF_DATA_PIN): cv.int_,
            cv.Required(CONF_CLOCK_PIN): cv.int_,
            cv.Optional(CONF_VOLTAGE): cv.string,
            #cv.Optional(CONF_GAIN): cv.string,
            #cv.Optional(CONF_RATE): cv.string,
        },
    )
    .extend(cv.polling_component_schema("3s"))
)

async def to_code(config):
    var = await sensor.new_sensor(config,config[CONF_DATA_PIN],config[CONF_CLOCK_PIN])
    if CONF_VOLTAGE in config:
        cg.add(var.set_voltage(config[CONF_VOLTAGE]))
    
    await cg.register_component(var, config)
    
