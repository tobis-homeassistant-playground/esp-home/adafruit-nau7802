#pragma once
#include <Adafruit_NAU7802.h>
#include "esphome/core/component.h"
#include "esphome/components/sensor/sensor.h"

namespace esphome {
  namespace nau7802_ {
    class Nau7802Sensor : public sensor::Sensor, public PollingComponent {
      public:
        Nau7802Sensor(const int data_pin, const int clock_pin);
        
        void dump_config() override;
        void update() override;

        void set_voltage(char* voltage);
      
      private:
        Adafruit_NAU7802 nau;
        int sda;
        int scl;

    };

  }  // namespace nau7802_
}  // namespace esphome
